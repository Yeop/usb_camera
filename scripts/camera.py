#!/usr/bin/env python

import cv2
import time

class Camera(object):
    def __init__(self, camID):

        #self.image_pub = rospy.Publisher("/camera/wide/compressed",
        #    CompressedImage)

        self._open_camera(camID)
        
    def _open_camera(self, camID):
        self.video_capture = cv2.VideoCapture(camID)


    def get_frame(self):
        if self.video_capture is not None and self.video_capture.isOpened():
            status, frame = self.video_capture.read()
            if status:
                yield frame

        while True:
            if self.video_capture is None:
                yield None
                break
            status, frame = self.video_capture.read()

            if status:
                yield frame
            
        self.close_camera()
        yield None


    def close_camera(self):
        self.video_capture.release()
        self.video_capture = None